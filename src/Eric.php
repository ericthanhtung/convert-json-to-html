<?php

namespace Eric\ConvertJsonToHtml;

class Eric
{
    public static function convertToHtml($jsonData)
    {
        $datas = (array)json_decode($jsonData);
        $root = $datas['ROOT'];
        try {
            $html = "<div id='ROOT'>";
            $html .= self::genHtml($datas, $root->nodes);
            $script = self::genScript($datas, $root->nodes);
            $html .= "</div>";
            $style = self::genStyle($datas);

            return self::genFile($style, $html, $script);

        } catch (\Exception $e) {
            dd($e);
        }

    }

    public static function changeLatterSnakey($text)
    {
        return strtolower(preg_replace('/(?<!\ )[A-Z]/', '-$0', $text));
    }

    public static function checkUppercase($text)
    {
        return preg_match('/[A-Z]/', $text);
    }

    public static function convertKeyCSS(array $arr)
    {
        $k = "";
        $v = [];
        foreach ($arr as $key => $value) {
            $k .= $key;
            $v[] = $value;
        }

        return $k . '(' . implode(',', $v) . ')';
    }

    public static function genHtml($datas, $nodes)
    {
        $html = '';
        foreach ($nodes as $node) {
            if ($datas[$node]->type->resolvedName == 'Text') {
                $text = $datas[$node]->props->text;
                $html .= <<<HTML
                    <p id="$node">$text</p>
                HTML;
            }
            if ($datas[$node]->type->resolvedName == 'Button') {
                $textBtn = $datas[$node]->props->text;
                $html .= <<<HTML
                    <button id="$node">$textBtn</button>
                HTML;
            }
            if ($datas[$node]->type->resolvedName == 'Container') {
                $child = '';
                if (!empty($datas[$node]->nodes) && $datas[$node]->hidden == false) {
                    $child = self::genHtml($datas, $datas[$node]->nodes);
                }
                $html .= <<<HTML

                    <div id="$node">
                        $child
                    </div>

                HTML;
            }
            if ($datas[$node]->type->resolvedName == 'Video') {
                $link = $datas[$node]->props->videoId;
                $html .= <<<HTML
                    <iframe width='100%' height='100%' id='$node' src='https://www.youtube.com/embed/$link'></iframe>
                HTML;
            }
            if ($datas[$node]->type->resolvedName == 'Embeded') {
                $src = $datas[$node]->props->src;
                $html .= <<<HTML
                    <iframe width='100%' height='100%' id='$node' src='$src'></iframe>
                HTML;

            }
            if ($datas[$node]->type->resolvedName == 'Image') {
                $url = $datas[$node]->props->url;
                $width = $datas[$node]->props->width;
                $height = $datas[$node]->props->height;
                $html .= <<<HTML
                    <img width="$width" height="$height" id="$node" src="$url">
                HTML;
            }

            if ($datas[$node]->type->resolvedName == 'Link') {
                $href = $datas[$node]->props->href;
                $title = $datas[$node]->props->title;
                $html .= <<<HTML
                    <a href='$href' id='$node'>$title</a>
                HTML;
            }

            if ($datas[$node]->type->resolvedName == 'CustomHTML') {
                $customHTML = $datas[$node]->props->html;
                $html .= <<<HTML
                    $customHTML
                HTML;
            }

            if ($datas[$node]->type->resolvedName == 'TabsComponent') {
                $html .= '<div role="tablist" class="tab-container">';
                foreach ($datas[$node]->linkedNodes as $tab) {
                    $html .= <<<HTML

                        <div role="tab" aria-controls="$tab" class="tab">
                            <span>Tab $tab</span>
                        </div>

                    HTML;
                }
                $html .= <<<HTML
                    </div>
                    <div class="tab-contents">
                   HTML;
                foreach ($datas[$node]->linkedNodes as $tab) {
                    $child = 'Nội dung';
                    if (!empty($datas[$tab]->nodes) && $datas[$tab]->hidden == false) {
                        $child = self::genHtml($datas, $datas[$tab]->nodes);
                    }
                    $html .= <<<HTML

                        <div role="tabpanel" id="$tab" hidden class="tab-content">
                            <div>
                                $child
                            </div>
                        </div>
                    HTML;
                }
                $html .= <<<HTML
                </div>
                HTML;
            }

            if ($datas[$node]->type->resolvedName == 'Slide') {
                $htmlSlide = '';

                foreach ($datas[$node]->linkedNodes as $slide) {
                    if (!empty($datas[$slide]->linkedNodes) && $datas[$slide]->hidden == false) {
                        $child = self::genHtml($datas, [$slide]);
                        $htmlSlide .= <<<HTML

                            <div class="swiper-slide">$child</div>

                        HTML;
                    }
                }

                $html .= <<<HTML

                    <div class="swiper swiper-$node" id="$node">
                      <div class="swiper-wrapper">
                        $htmlSlide
                      </div>
                      <div class="swiper-button-next"></div>
                      <div class="swiper-button-prev"></div>
                      <div class="swiper-pagination"></div>
                    </div>

                HTML;

            }
        }
        return $html;
    }

    public static function genStyle($datas)
    {
        $style = '<style>';
        foreach ($datas as $key => $data) {
            $style .= "#{$key} {";
            foreach ($data->props as $attribute => $value) {
                if (self::checkUppercase($attribute) == true) {
                    $attribute = self::changeLatterSnakey($attribute);
                }
                if (is_array($value)) {
                    $style .= $attribute . ':' . implode('px ', $value) . "px;";
                } elseif (is_object($value)) {
                    $value = (array)$value;
                    $style .= $attribute . ':' . self::convertKeyCSS($value) . ";";
                } elseif (in_array($attribute, ['font-size'])) {
                    $style .= "$attribute: $value"."px;";
                } elseif (in_array($attribute, ['text', 'url', 'video-id', 'html', 'autoplay', 'effect', 'slidesPerView'. 'numberOfslide', 'numberOfTabs'])) {
                    continue;
                } else {
                    $style .= "$attribute: $value;";
                }
            }
            $style .= "}";
        }
        $style .= '</style>';
        return $style;
    }

    public static function genScript($datas, $nodes) {
        $script = <<<JS
            <script>
            JS;
        foreach ($nodes as $node) {
            if ($datas[$node]->type->resolvedName == 'TabsComponent') {
                $parent = $datas[$node]->parent;
                $script .= <<<JS

                    var props = {
                        "$parent":{
                            "classactive":"tab-active","selectortab":"aria-controls"}
                    };
                    var ids = Object.keys(props).map(function(id) {
                        return '#'+id }
                    ).join(',');
                    var els = document.querySelectorAll(ids);
                    for (var i = 0, len = els.length; i < len; i++) {
                        var el = els[i];
                        (function(t){
                        var e,n,r=this,o=t.classactive,a=t.selectortab,c=window,i=c.history,s=c._isEditor,b='[role=tab]',p=document,l=p.body,u=p.location,f=l.matchesSelector||l.webkitMatchesSelector||l.mozMatchesSelector||l.msMatchesSelector,y=function(t,e){
                        for(var n=t||[],r=0;r<n.length;r++)e(n[r],r)}
                        ,d=function(t){
                        return t.getAttribute(a)}
                        ,O=function(t,e){
                        return t.querySelector(e)}
                        ,g=function(){
                        return r.querySelectorAll(b)}
                        ,j=function(t,e){
                        return!s&&(t.tabIndex=e)}
                        ,h=function(t){
                        y(g(),(function(t){
                                t.className=t.className.replace(o,'').trim(),t.ariaSelected='false',j(t,'-1')}
                        )),y(r.querySelectorAll("[role=tabpanel]"),(function(t){
                                return t.hidden=!0}
                        )),t.className+=' '+o,t.ariaSelected='true',j(t,'0');
                        var e=d(t),n=e&&O(r,"#".concat(e));
                        n&&(n.hidden=!1)}
                        ,v=O(r,".".concat(o).concat(b));
                        (v=v||(n=(u.hash||'').replace('#',''))&&O(r,(e=a,"".concat(b,"[").concat(e,"=").concat(n,"]")))||O(r,b))&&h(v),r.addEventListener('click',(function(t){
                        var e=t.target,n=f.call(e,b);
                        if(n||(e=function(t){
                            var e;
                            return y(g(),(function(n){
                                    e||n.contains(t)&&(e=n)}
                            )),e}
                        (e))&&(n=1),n&&!t.__trg&&e.className.indexOf(o)<0){
                            t.preventDefault(),t.__trg=1,h(e);
                            var r=d(e);
                            try{
                                i&&i.pushState(null,null,"#".concat(r))}
                            catch(t){
                            }
                        }
                    }
                    ))}
                    .bind(el))(props[el.id]);
                    }
                JS;
            }
            if ($datas[$node]->type->resolvedName == 'Slide') {
                $autoPlay = $datas[$node]->props->autoplay * 1000;
                $effect = $datas[$node]->props->effect;
                $slidesPerView = $datas[$node]->props->slidesPerView;
                $script .= <<<JS
                    var swiper = new Swiper(".swiper-$node", {
                        effect: $effect,
                        slidesPerView: $slidesPerView,
                        navigation: {
                          nextEl: ".swiper-button-next",
                          prevEl: ".swiper-button-prev",
                        },
                        autoplay: {
                           delay: $autoPlay
                        },
                        pagination: {
                            el: ".swiper-pagination",
                            dynamicBullets: true,
                        },
                    });
                JS;
            }
        }
        $script .=  <<<JS

            </script>
            JS;
        return $script;
    }

    public static function  genFile($style, $html, $script = null)
    {
        $pagecontents = file_get_contents(__DIR__ . "/index.html");
        $pagecontents =  str_replace("{{style}}", $style, $pagecontents);
        $pagecontents =  str_replace("{{body}}", $html, $pagecontents);
        $pagecontents =  str_replace("{{script}}", $script, $pagecontents);

        $a = fopen("page-build.html", 'w');
        fwrite($a, $pagecontents);
        fclose($a);
        chmod("page-build.html", 0644);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename('page-build.html').'"');
        header('Content-Length: ' . filesize('page-build.html'));
        header('Pragma: public');
        flush();
        readfile('page-build.html',true);
        die();
    }
}
